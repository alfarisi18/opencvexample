from sklearn import svm
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from sklearn.metrics import classification_report, confusion_matrix

from matplotlib import pyplot as plt
from sklearn.decomposition import PCA



df = pd.read_csv('result/features.csv')

X = df[['contrast', 'energy', 'homo', 'corr']]
y = df['label']

pca = PCA(n_components=2)
principalComponents = pca.fit_transform(X)

principalDf = pd.DataFrame(data = principalComponents
             , columns = ['principal component 1', 'principal component 2'])

finalDf = principalDf
finalDf['label'] = y.tolist()

print(finalDf)

fig = plt.figure(figsize = (8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xlabel('Nilai GLCM', fontsize = 15)
ax.set_ylabel('Label Penyakit', fontsize = 15)
ax.set_title('2 component PCA', fontsize = 20)
targets = ['bercak karat','lanas', 'sehat']
colors = ['r', 'g' , 'b']
for target, color in zip(targets,colors):
    indicesToKeep = finalDf['label'] == target
    ax.scatter(finalDf.loc[indicesToKeep, 'principal component 1']
               , finalDf.loc[indicesToKeep, 'principal component 2']
               , c = color
               , s = 50)
ax.legend(targets)
ax.grid()
plt.show()