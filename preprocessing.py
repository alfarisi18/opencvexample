import numpy as np
import cv2 as cv
from os import listdir
import pandas as pd
import re
import os
from skimage.feature import greycomatrix
from skimage.feature import greycoprops

contrast = []
energy = []
homo = []
corr = []
filename = []
label = []

mypath = './gambar/'
kelas = [f for f in os.listdir(mypath)]
kelas = kelas[1:]
for klas in kelas:
    files = [f for f in os.listdir(mypath + klas + '/')]
    print(files)
    for file in files:
        img = cv.imread(mypath + klas + '/' + file)
        filename.append(file)
        label.append(klas)

        # ==== resize image =======
        print(file)
        dim = (96, 96)
        img = cv.resize(img, dim, interpolation = cv.INTER_AREA)

        # ==== remove background grabcut=====
        mask = np.zeros(img.shape[:2],np.uint8)
        bgdModel = np.zeros((1,65),np.float64)
        fgdModel = np.zeros((1,65),np.float64)
        rect = (1,1,290,290)
        cv.grabCut(img,mask,rect,bgdModel,fgdModel,5,cv.GC_INIT_WITH_RECT)
        mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
        img = img*mask2[:,:,np.newaxis]

        # ==== grayscale ====
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)


        # ==== save new image =====
        cv.imwrite('result/' + klas +'/' + file, gray)


        
        # ===== GLCM ====
        glcm = greycomatrix(gray, [5], [0], 256, symmetric=True, normed=True)
        contrast.append(greycoprops(glcm, 'contrast')[0, 0])
        energy.append(greycoprops(glcm, 'energy')[0, 0])
        homo.append(greycoprops(glcm, 'homogeneity')[0, 0])
        corr.append(greycoprops(glcm, 'correlation')[0, 0])

# ===== create feature.csv ====
df = pd.DataFrame()
df['filename'] = filename
df['contrast'] = contrast
df['energy'] = energy
df['homo'] = homo
df['corr'] = corr
df['label'] = label

df.to_csv('result/features.csv')